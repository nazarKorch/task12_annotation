import annotation.FieldAnnotation;
import annotation.ForRunning;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import sample.Receiver;
import sample.SampleClass;

public class Main {

  public static void main(String[] args) {

    SampleClass.showAnnotationFields();
    SampleClass.showField();
    try {
      run();
    } catch (Exception e) {
      e.printStackTrace();
    }

    SampleClass.showField();

    SampleClass sampleClass = new SampleClass();
    Receiver.showInfoAboutClass(sampleClass);


  }

  public static void run() throws Exception {
    SampleClass sampleClass = new SampleClass();
    Method[] methods = SampleClass.class.getMethods();
    Field field = SampleClass.class.getDeclaredField("someField");

    for (Method m : methods) {

      if (m.isAnnotationPresent(ForRunning.class)) {
        if (m.getAnnotation(ForRunning.class).number() == 1) {
          m.invoke(SampleClass.class, 2);
        } else if (m.getAnnotation(ForRunning.class).number() == 2) {
          m.invoke(SampleClass.class, "Nazar");
        } else if (m.getAnnotation(ForRunning.class).number() == 3) {
          Object o = m.invoke(SampleClass.class, null);
          System.out.println("third returned: " + o);
        }
      } else if (m.getName() == "myMethod") {
        if (m.getParameterTypes().length > 1) {
          int[] numbers = {2, 3, 4};
          m.invoke(SampleClass.class, "string", numbers);
        } else {
          String[] strings = {"first str", "second str"};
          m.invoke(SampleClass.class, new Object[]{strings});
        }
      }
    }

    if (field.isAnnotationPresent(FieldAnnotation.class)) {
      field.set(sampleClass, 5);

    }
  }


}
