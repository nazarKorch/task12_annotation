package sample;

import annotation.FieldAnnotation;
import annotation.ForRunning;
import annotation.MyAnnotation;
import java.lang.annotation.Annotation;
import java.util.Arrays;

@MyAnnotation(authorName = "Nazar", authorAge = 22)
public class SampleClass {

  @FieldAnnotation
  public static int someField = 2;

  public static void showField(){
    System.out.println("someField value: " + someField);

  }
  public static void showAnnotationFields() {

    Annotation[] annotations = SampleClass.class.getAnnotations();
//    for (Annotation a : annotations) {
//      System.out.println(a.annotationType());
//    }
    System.out.println(Arrays.toString(annotations));

  }

  @ForRunning
  public static void firstMethod(int i) {
    System.out.println("first invoked");
  }

  @ForRunning(number = 2)
  public static void secondMethod(String str) {
    System.out.println("second invoked, argument: " + str);
  }

  @ForRunning(number = 3)
  public static int thirdMethod() {
    System.out.println("third invoked ");
    return 1;
  }


  public static void myMethod(String s, int... args) {
    System.out.println("inside myMethod: " + s + " " + Arrays.toString(args));
  }

  public static void myMethod(String... args){
    System.out.println("inside myMethod: " + Arrays.toString(args));
  }


}
