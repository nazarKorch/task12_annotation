package sample;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Receiver {

  public static void showInfoAboutClass(Object object) {
    System.out.println("Name of object: " + object.getClass().getSimpleName());
    System.out.println("Information about annotations: ");
    if (object.getClass().getAnnotations() != null) {
      for (Annotation a : object.getClass().getDeclaredAnnotations()) {
        System.out.println(a.toString());
      }
    }

    if (object.getClass().getFields() != null) {
      System.out.println("Information about fields: ");
      for (Field f : object.getClass().getFields()) {
        System.out.println(f.getName() + " (" + f.toString() + ") "
            + Arrays.toString(f.getDeclaredAnnotations()) + " " );
      }
    }

    if (object.getClass().getMethods() != null) {
      System.out.println("Information about methods: ");
      for (Method m : object.getClass().getMethods()) {
        System.out.println(m.getName() + " " + Arrays
            .toString(m.getParameterTypes()) + " " + m.getReturnType());
      }
    }

  }

}
